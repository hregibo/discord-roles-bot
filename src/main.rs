extern crate discord;

use discord::model::{Event, MessageId, ReactionEmoji, RoleId, ServerId};
use discord::Discord;
use std::env;

const SERVER_ID: ServerId = ServerId(434747019945312297);
const MESSAGE_ID: MessageId = MessageId(823282699883773993);

fn find_matching_role(emoji: &ReactionEmoji) -> Option<RoleId> {
    return match emoji {
        ReactionEmoji::Custom { name, id: _ } => {
            if name.eq("lumiLL") {
                return Some(RoleId(697046356010991728));
            } else if name.eq("aenoanLUL") {
                return Some(RoleId(794940901042552842));
            } else if name.eq("lumiAYAYA") {
                return Some(RoleId(794966500577509406));
            }
            None
        }
        _ => None,
    };
}

fn main() {
    let discord = Discord::from_bot_token(&env::var("DISCORD_TOKEN").expect("Expected token"))
        .expect("Login failed!");

    let (mut connection, _) = discord.connect().expect("Connect failed.");
    println!("Ready to process events.");
    // setting activity for Discord bot
    connection.set_game_name("with your heart".to_owned());

    loop {
        match connection.recv_event() {
            Ok(Event::ReactionAdd(reaction)) => {
                if reaction.message_id != MESSAGE_ID {
                    continue;
                }
                let emoji = &reaction.emoji;
                let role = find_matching_role(emoji);
                if role.is_none() {
                    continue;
                }
                let role_id = role.unwrap();
                println!(
                    "user {} reacted with {:?}, giving himself role #{}",
                    reaction.user_id, reaction.emoji, role_id,
                );

                discord
                    .add_member_role(SERVER_ID, reaction.user_id, role_id)
                    .err();
            }
            Ok(Event::ReactionRemove(reaction)) => {
                if reaction.message_id != MESSAGE_ID {
                    continue;
                }

                let emoji = &reaction.emoji;
                let role = find_matching_role(emoji);
                if role.is_none() {
                    continue;
                }
                let role_id = role.unwrap();
                println!(
                    "user {} cancelled reaction {:?}, removing role #{}",
                    reaction.user_id, reaction.emoji, role_id,
                );

                discord
                    .remove_member_role(SERVER_ID, reaction.user_id, role_id)
                    .err();
            }
            Ok(_) => {}
            Err(discord::Error::Closed(code, body)) => {
                println!("Gateway closed on us with code {:?}: {}", code, body);
                break;
            }
            Err(err) => println!("Received error:{:?}", err),
        }
    }
}
