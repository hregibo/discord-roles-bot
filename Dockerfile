FROM rust:latest AS builder
WORKDIR /app
COPY . .
RUN cargo build --release

FROM debian:stable-slim AS runner

WORKDIR /app
COPY --from=builder /app/target/release/discord-roles .
RUN apt-get update && apt-get install -y ca-certificates libc6-dev
CMD ["/app/discord-roles"]
